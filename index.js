var express = require('express');
var app = express();
var PubNub = require('pubnub');
//Adding Request and Keen code
var request = require('request');
var KeenTracking = require('keen-tracking');


app.set('port', (process.env.PORT || 5000));

app.use(express.static(__dirname + '/public'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
  response.render('pages/index');
});

app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'));
});

//Feeding Data to Keen

// Configure a client instance
var client = new KeenTracking({
  projectId: '59642588c9e77c000128d60f',
  writeKey: '9CC6EA74468AC36FFD3AB63E78C0F0BA2EA980A4C186286461B088317CC004895D51057AF804776E83F786116BFF7B1B68F640AB5215EA7D607EE5D31FFF9661DE265135DF5DC4B4B0F235C5DFDD9AD0EAC0EA3F01B9D2A199F48009E9CFB88D'
});

var pubnub = new PubNub({
    subscribeKey: 'sub-c-85f5b30c-6c47-11e7-84e1-0619f8945a4f', // always required
    publishKey: 'pub-c-28e5b25b-e889-4d15-9b3b-0f47a4c9086e' // only required if publishing
});

 

function request1() {
	
//Request hashrate
  request('https://zen.suprnova.cc/index.php?page=api&action=getuserstatus&api_key=ff59834c6df278f71492e471feea93048631b4261c6dfcd93babd33691b47d5c&id=950896', function (error, response, body) {
  if (!error && response.statusCode == 200) {
    
	var obj = JSON.parse(body);
	console.log(obj.getuserstatus.data.hashrate);
	client.recordEvent('hashrate', {
	  speed: ((obj.getuserstatus.data.hashrate)/1000)
	});
  }
});


//Request User Balance
request('https://zen.suprnova.cc/index.php?page=api&action=getuserbalance&api_key=ff59834c6df278f71492e471feea93048631b4261c6dfcd93babd33691b47d5c&id=950896', function (error, response, body) {
  if (!error && response.statusCode == 200) {
    
	var obj = JSON.parse(body);
	console.log(obj.getuserbalance.data.confirmed);
	console.log(obj.getuserbalance.data.unconfirmed);
	client.recordEvent('balance', {
	  confirmed: obj.getuserbalance.data.confirmed,
	  unconfirmed: obj.getuserbalance.data.unconfirmed
	});
  }
});
}
setInterval(request1, 60000);



